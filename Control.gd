extends Control


func _draw():
	for ship in get_tree().get_nodes_in_group("ships"):
		var points_arc = ship.points_arc
		for index_point in range(points_arc.size()-1):
			draw_line(points_arc[index_point], points_arc[index_point + 1], Color.green)

func _physics_process(delta):
	update()
