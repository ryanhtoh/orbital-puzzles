tool
extends Node2D

export (float) var gravity
export (Texture) var texture setget set_texture

func _ready():
	$Sprite.texture = texture

func set_texture(tex):
	texture = tex
	if Engine.editor_hint:
		$Sprite.texture = texture
