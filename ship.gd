extends KinematicBody2D

var thrust = 15.0
var velocity = Vector2.ZERO
var G = 220.0
var time = 0.0
var points_arc = PoolVector2Array()
var PFPS
var vel_phase = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _physics_process(delta):
	PFPS = delta
	time += delta
	look_at(get_global_mouse_position())
	
	if Input.is_action_pressed("thrust"):
		$flame.show()
		velocity += delta * thrust * Vector2(cos(rotation), sin(rotation))
	else:
		$flame.hide()
	
	if vel_phase:
		vel_phase = false
		velocity = calc_gravity(velocity, global_position, delta)
	else:
		vel_phase = true
		global_position += velocity * delta
	
	if time > 0.125:
		time -= 0.125
		draw_orbit(3000)

func calc_gravity(vel, pos, delta):
	for body in get_tree().get_nodes_in_group("bodies"):
		var r = body.global_position - pos
		vel += delta * G * (body.gravity / r.length_squared()) * r.normalized()
	return vel


func draw_orbit(nb_points):
	points_arc = PoolVector2Array()
	var next_vel = velocity
	var next_pos = global_position

	for i in range(nb_points + 1):
		points_arc.push_back(next_pos)
		next_vel = calc_gravity(next_vel, next_pos, PFPS)
		next_pos += next_vel * PFPS
